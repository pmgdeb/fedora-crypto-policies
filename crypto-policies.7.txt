////
Copyright (C) 2019, 2020 Red Hat, Inc.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
////


crypto-policies(7)
==================
:doctype: manpage
:man source: crypto-policies


NAME
----
crypto-policies - system-wide crypto policies overview


DESCRIPTION
-----------
The security of cryptographic components of the operating system
does not remain constant over time. Algorithms, such as
cryptographic hashing and encryption, typically have a lifetime,
after which they are considered either too risky to use or plain insecure.
That means, we need to phase out such algorithms from the default
settings or completely disable them if they could cause an irreparable problem.

While in the past the algorithms were not disabled in a consistent way
and different applications applied different policies, the system-wide
crypto-policies followed by the crypto core components allow consistently
deprecating and disabling algorithms system-wide.

The individual policy levels (*DEFAULT*, *LEGACY*, *FUTURE*, and *FIPS*)
are included in the *crypto-policies(7)* package. In the future, there will
be also a mechanism for easy creation and deployment of policies defined
by the system administrator or a third party vendor.

For rationale, see *RFC 7457* for a list of attacks taking advantage of
legacy crypto algorithms.

COVERED APPLICATIONS
--------------------

Crypto-policies apply to the configuration of the core cryptographic subsystems,
covering *TLS*, *IKE*, *IPSec*, *DNSSec*, and *Kerberos* protocols; i.e., the
supported secure communications protocols on the base operating system.

Once an application runs in the operating system, it follows the default
or selected policy and refuses to fall back to algorithms and protocols not
within the policy, unless the user has explicitly requested the application
to do so. That is, the policy applies to the default behavior of applications
when running with the system-provided configuration but the user can override
it on an application-specific basis.

The policies currently provide settings for these applications and libraries:

* *BIND* DNS name server daemon
* *GnuTLS* TLS library
* *OpenJDK* runtime environment
* *Kerberos 5* library
* *Libreswan* IPsec and IKE protocol implementation
* *NSS* TLS library
* *OpenSSH* SSH2 protocol implementation
* *OpenSSL* TLS library
* *libssh* SSH2 protocol implementation

Applications using the above libraries and tools are covered by the
cryptographic policies unless they are explicitly configured not to be so.

PROVIDED POLICY LEVELS
----------------------

*LEGACY*::
  This policy ensures maximum compatibility with legacy systems; it is
  less secure and it includes support for *TLS 1.0*, *TLS 1.1*, and *SSH2*
  protocols or later. The algorithms *DSA* and *3DES* are allowed,
  while *RSA* and *Diffie-Hellman* parameters are accepted if larger than 1023 bits.
  The level provides at least 64-bit security.

  * MACs: all *HMAC* with *SHA-1* or better + all modern MACs (*Poly1305*
    etc.)
  * Curves: all prime >= 255 bits (including Bernstein curves)
  * Signature algorithms: with *SHA1* hash or better (*DSA* allowed)
  * *TLS* Ciphers: all available >= 112-bit key, >= 128-bit block (including
    *3DES*, excluding *RC4*)
  * Non-TLS Ciphers: same as *TLS* ciphers with added *Camellia*
  * Key exchange: *ECDHE*, *RSA*, *DHE*
  * *DH* params size: >= 1023
  * *RSA* keys size: >= 1023
  * *DSA* params size: >= 1023
  * *TLS* protocols: *TLS* >= 1.0, *DTLS* >= 1.0

*DEFAULT*::
  The *DEFAULT* policy is a reasonable default policy for today's standards.
  It allows the *TLS 1.2*, and *TLS 1.3* protocols,
  as well as *IKEv2* and *SSH2*. The *Diffie-Hellman*
  parameters are accepted if they are at least 2048 bits long.
  The level provides at least 112-bit security with the exception of allowing
  *SHA-1* signatures in DNSSec where they are still prevalent.

  * MACs: all *HMAC* with *SHA-1* or better + all modern MACs (*Poly1305*
    etc.)
  * Curves: all prime >= 255 bits (including Bernstein curves)
  * Signature algorithms: with *SHA-224* hash or better (no *DSA*)
  * *TLS* Ciphers: >= 128-bit key, >= 128-bit block (*AES*, *ChaCha20*,
    including *AES-CBC*)
  * non-TLS Ciphers: as *TLS* Ciphers with added *Camellia*
  * key exchange: *ECDHE*, *RSA*, *DHE* (no *DHE-DSS*)
  * *DH* params size: >= 2048
  * *RSA* keys size: >= 2048
  * *TLS* protocols: *TLS* >= 1.2, *DTLS* >= 1.2

*NEXT*::
  The *NEXT* policy is just an alias to the *DEFAULT* policy.

*FUTURE*::
  A conservative security level that is believed to withstand any near-term
  future attacks. This level does not allow the use of *SHA-1* in signature
  algorithms. The level also provides some (not complete) preparation for
  post-quantum encryption support in form of 256-bit symmetric encryption
  requirement. The *RSA* and *Diffie-Hellman* parameters are accepted if
  larger than 3071 bits. The level provides at least 128-bit security.

  * MACs: all *HMAC* with *SHA-256* or better + all modern MACs (*Poly1305*
    etc.)
  * Curves: all prime >= 255 bits (including Bernstein curves)
  * Signature algorithms: with *SHA-256* hash or better (no *DSA*)
  * *TLS* Ciphers: >= 256-bit key, >= 128-bit block, only Authenticated
    Encryption (AE) ciphers
  * non-TLS Ciphers: same as *TLS* ciphers with added non AE ciphers and
    *Camellia*
  * key exchange: *ECDHE*, *DHE* (no *DHE-DSS*, no *RSA*)
  * *DH* params size: >= 3072
  * *RSA* keys size: >= 3072
  * *TLS* protocols: *TLS* >= 1.2, *DTLS* >= 1.2


*FIPS*::
  A level that conforms to the *FIPS 140-2* requirements. This policy is used
  internally by the *fips-mode-setup(8)* tool which can switch the system
  into the *FIPS 140-2* compliance mode.
  The level provides at least 112-bit security.

  * MACs: all *HMAC* with *SHA1* or better
  * Curves: all prime >= 256 bits
  * Signature algorithms: with *SHA-256* hash or better (no *DSA*)
  * *TLS* Ciphers: >= 128-bit key, >= 128-bit block (*AES*,
    including *AES-CBC*)
  * non-TLS Ciphers: same as *TLS* Ciphers
  * key exchange: *ECDHE*, *DHE* (no *DHE-DSS*, no *RSA*)
  * *DH* params size: >= 2048
  * *RSA* params size: >= 2048
  * *TLS* protocols: *TLS* >= 1.2, *DTLS* >= 1.2

*EMPTY*::
  All cryptographic algorithms are disabled (used for debugging only, do not
  use).

CRYPTO POLICY DEFINITON FORMAT
------------------------------

The crypto policy definiton files have a simple syntax following an *INI* file
'key' = 'value' syntax with these particular features:

  * Comments are indicated by '#' character. Everything on the line following the
    character is ignored.
  * Backslash '\' character followed immediately with the end-of-line character
    indicates line continuation. The following line is concatenated to the current
    line after the backslash and end-of-line characters are removed.
  * Value types can be either decimal integers, arbitrary strings, or lists of
    strings without whitespace characters separated by any number of whitespaces.

The allowed keys are:

  * *mac*: List of allowed MAC algorithms
  * *ssh_group*: Optional; list of allowed groups or elliptic curves for key exchanges
    for use with the SSH protocol. If absent, the value is derived from *group*.
  * *group*: List of allowed groups or elliptic curves for key exchanges
    for use with other protocols
  * *hash*: List of allowed cryptographic hash (message digest) algorithms
  * *sign*: List of allowed signature algorithms
  * *tls_cipher*: Optional; list of allowed symmetric encryption algorithms (including the modes)
    for use with the TLS protocol. If absent, the value is derived from *cipher*.
  * *ssh_cipher*: Optional; list of allowed symmetric encryption algorithms (including the modes)
    for use with the SSH protocol. If absent, the value is derived from *cipher*.
  * *cipher*: List of allowed symmetric encryption algorithms (including the modes)
    for use with other protocols
  * *key_exchange*: List of allowed key exchange algorithms
  * *protocol*: List of allowed TLS and DTLS protocol versions (ignored by *OpenSSL*
    and *NSS* back ends)
  * *ike_protocol*: List of allowed IKE protocol versions
  * *min_tls_version*: Lowest allowed TLS protocol version (used only by *OpenSSL* a
    and *NSS* back ends)
  * *min_dtls_version*: Lowest allowed DTLS protocol version (used only by *NSS* back
    end)
  * *min_dh_size*: Integer value of minimum number of bits of parameters for *DH*
    key exchange
  * *min_dsa_size*: Integer value of minimum number of bits for *DSA* keys
  * *min_rsa_size*: Integer value of minimum number of bits for *RSA* keys
  * *sha1_in_certs*: Value of 1 if *SHA1* allowed in certificate signatures,
    0 otherwise (Applies to *GnuTLS* back end only.)
  * *sha1_in_dnssec*: Value of 1 if *SHA1* allowed in DNSSec protocol even if it is
    not present in the *hash* and *sign* lists, 0 otherwise (Applies to *BIND* back
    end only.)
  * *arbitrary_dh_groups*: Value of 1 if arbitrary group in
    *Diffie-Hellman* is allowed, 0 otherwise
  * *ssh_certs*: Value of 1 if *OpenSSH* certificate authentication is
     allowed, 0 otherwise
  * *ssh_etm*: Value of 1 if *OpenSSH* EtM (encrypt-then-mac) extension is
     allowed, 0 otherwise

The full policy definition files have suffix `.pol`, the policy module definition
files have suffix `.pmod`. The policy module files do not have to have values
set for all the keys listed above.

The lists as set in the base (full policy) are modified by the lists specified in the
module files in following way:

  * *-*'list-item': The 'list-item' is removed from the list specified in the base
    policy.
  * *+*'list-item': The 'list-item' is inserted at the beginning of the list specified
    in the base policy. The inserts are done in the order of appearance in the
    policy module file so the actual order in the final list will be reversed.
  * 'list-item'*+*: The 'list-item' is appended to the end of the list specified
    in the base policy.

To completely override a list value in a module file just use 'list-items' without any
sign. Combining 'list-items' with and without signs in a single list value assignment is
not allowed however an existing list value can be modified in multiple further assignments.

Non-list key values in the policy module files are simply overriden.

The keys marked as *Optional* can be omitted in the policy definition
files. In that case, the values will be derived from the base
keys. Note that, this value propagation only applies to the policy
definition files. In the policy module files, each key that needs
modification must be explicitly specified.

*Policy file placement and naming:*

The policy files shipped in packages are placed in
`/usr/share/crypto-policies/policies` and the policy modules in
`/usr/share/crypto-policies/policies/modules`.

The locally configured policy files are placed in
`/etc/crypto-policies/policies` and the policy modules in
`/etc/crypto-policies/policies/modules`.

The policy and policy module files must have names in upper-case except
for the `.pol` and `.pmod` suffix as the update-crypto-policies command
always converts the policy name to upper-case before searching for
the policy on the filesystem.

COMMANDS
--------

*update-crypto-policies(8)*::
  This command manages the policies available to the various cryptographic
  back ends and allows the system administrator to change the active
  cryptographic policy level.

*fips-mode-setup(8)*::
  This command allows the system administrator to enable, or disable the
  system FIPS mode and also apply the *FIPS* cryptographic policy level
  which limits the allowed algorithms and protocols to these allowed by
  the FIPS 140-2 requirements.


NOTES
-----

*Exceptions*

* *Go-language* applications do not yet follow the system-wide policy.
* *GnuPG-2* application does not follow the system-wide policy.

In general only the data-in-transit is currently covered by the system-wide
policy.

If the system administrator changes the system-wide policy level with
the *update-crypto-policies(8)* command it is advisable to restart the
system as the individual back-end libraries read the configuration files
usually during their initialization. The changes in the policy level
thus take place in most cases only when the applications using the back-end
libraries are restarted.

*Removed cipher suites and protocols*

The following cipher suites and protocols are completely removed from the
core cryptographic libraries listed above:

* *DES*
* All export grade cipher suites
* *MD5* in signatures
* *SSLv2*
* *SSLv3*
* All *ECC* curves smaller than 224 bits
* All binary field *ECC* curves

*Cipher suites and protocols disabled in all policy levels*

The following ciphersuites and protocols are available but disabled in all
crypto policy levels:

* *DH* with parameters < 1024 bits
* *RSA* with key size < 1024 bits
* *Camellia*
* *RC4*
* *ARIA*
* *SEED*
* *IDEA*
* Integrity only ciphersuites
* *TLS* *CBC mode* ciphersuites using *SHA-384* HMAC
* *AES-CCM8*
* all *ECC* curves incompatible with *TLS 1.3*, including secp256k1
* *IKEv1*

*Notable irregularities in the individual configuration generators*

* *OpenSSL*: The minimum length of the keys and some other parameters
are enforced by the @SECLEVEL value which does not provide a fine
granularity. The list of *TLS* ciphers is not generated as an exact list
but by subtracting from all the supported ciphers for the enabled key
exchange methods. For that reason there is no way to disable a random cipher.
In particular all *AES-128* ciphers are disabled if the *AES-128-GCM* is not
present in the list; all *AES-256* ciphers are disabled if the *AES-256-GCM*
is not present. The *CBC* ciphers are disabled if there isn't *HMAC-SHA1*
in the hmac list and *AES-256-CBC* in the cipher list. To disable the *CCM*
ciphers both *AES-128-CCM* and *AES-256-CCM* must not be present in the cipher
list.

* *GnuTLS*: The minimum length of the keys and some other parameters
are enforced by min-verification-profile setting in the *GnuTLS* configuration
file which does not provide fine granularity.

* *OpenSSH*: *DH* group 1 is always disabled on server even if the policy
allows 1024 bit *DH* groups in general. The OpenSSH configuration option
HostKeyAlgorithms is set only for the *SSH* server as otherwise the
handling of the existing known hosts entries would be broken on client.

* *Libreswan*: The *key_exchange* parameter does not affect the generated
configuration. The use of regular *DH* or *ECDH* can be limited with appropriate
setting of the *group* parameter.

HISTORY
-------

The *ECDHE-GSS* and *DHE-GSS* algorithms are newly introduced and must be
specified in the base policy for the SSH GSSAPI key exchange methods
to be enabled. Previously the legacy SSH GSSAPI key exchange methods were
automatically enabled when the *SHA1* hash and *DH* parameters of at least
2048 bits were enabled.

Before the introduction of the *custom crypto policies* support it
was possible to have an completely arbitrary crypto policy created as
a set of arbitrary back-end config files in
`/usr/share/crypto-policies/<POLICYNAME>` directory. With the introduction
of the *custom crypto policies* it is still possible but there must
be an empty (possibly with any comment lines) `<POLICYNAME>.pol` file
in `/usr/share/crypto-policies/policies` so the `update-crypto-policies`
command can recognize the arbitrary custom policy.

FILES
-----
/etc/crypto-policies/back-ends::
	The individual cryptographical back-end configuration files.
	Usually linked to the configuration shipped in the crypto-policies
	package unless a configuration from `local.d` is added.

/etc/crypto-policies/config::
	The active crypto-policies level set on the system.

/etc/crypto-policies/local.d::
	Additional configuration shipped by other packages or created by
	the system administrator. The contents of the `<back-end>-file.config`
	is appended to the configuration from the policy back end as
	shipped in the crypto-policies package.

/usr/share/crypto-policies/policies::
	System policy definition files.

/usr/share/crypto-policies/policies/modules::
	System subpolicy module definition files.

/etc/crypto-policies/policies::
	Custom policy definition files as configured by the system
	administrator.

/etc/crypto-policies/policies/modules::
	Custom subpolicy module definition files as configured by the system
	administrator.

/usr/share/crypto-policies/<'POLICYNAME'>::
	Pre-generated back-end configurations for policy 'POLICYNAME'.

SEE ALSO
--------
update-crypto-policies(8), fips-mode-setup(8)


AUTHOR
------
Written by Tomáš Mráz.
